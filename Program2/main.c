#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fun.h"

void przypis(float *X, float *Y, float *RHO, FILE *plik);

int main()
{
    FILE *plik;
    plik=fopen("P0001_attr.rec","r+");
    float *X=(float *)malloc(51 * sizeof(float));
    float *Y = (float *)malloc(51 * sizeof(float));
    float *RHO = (float *)malloc(51 * sizeof(float));
    float *LP= (float *)malloc(51 * sizeof(float));

    przypis(X,Y,RHO,plik);

    float srx = srednia(X);
    float sry = srednia(Y);
    float srrho = srednia(RHO);

    float medx = mediana(X);
    float medy = mediana(Y);
    float medrho = mediana(RHO);

    float odchylx = odchylenie(X,srx);
    float odchyly = odchylenie(Y,sry);
    float odchylrho = odchylenie(RHO,srrho);

    printf("Srednia dla X:%.5f, dla Y:%.5f, dla RHO:%.3f\n",srx, sry, srrho);
    fprintf(plik,"\n\nSrednia dla X:%.5f, dla Y:%.5f, dla RHO:%.3f\n",srx, sry, srrho);
    printf("Mediana dla X:%.5f, dla Y:%.5f, dla RHO:%.3f\n",medx, medy, medrho);
    fprintf(plik,"Mediana dla X:%.5f, dla Y:%.5f, dla RHO:%.3f\n",medx, medy, medrho);
    printf("Odchylenie dla X:%.5f, dla Y:%.5f, dla RHO:%.3f\n",odchylx, odchyly, odchylrho);
    fprintf(plik,"Odchylenie dla X:%.5f, dla Y:%.5f, dla RHO:%.3f\n",odchylx, odchyly, odchylrho);
  
    free(X);
    free(Y);
    free(RHO);
    free(LP);
    fclose(plik);
    return(0);

}

void przypis(float *X, float *Y, float *RHO, FILE *plik)
{
    fseek(plik,16,SEEK_SET);
    for(int i=1; i<10; i++)
    {
    fscanf(plik, "%f", &X[i]);
    fseek(plik,2,SEEK_CUR);
    fscanf(plik,"%f", &Y[i]);
    fseek(plik,1,SEEK_CUR);
    fscanf(plik,"%f", &RHO[i]);
    fseek(plik,4,SEEK_CUR);
    printf("%d\t%.5f\t\t%.5f\t\t%.5f\n", i, X[i],Y[i],RHO[i]);
    }
    for(int i=10; i<51; i++)
    {
    fseek(plik,1,SEEK_CUR);
    fscanf(plik, "%f", &X[i]);
    fseek(plik,2,SEEK_CUR);
    fscanf(plik,"%f", &Y[i]);
    fseek(plik,1,SEEK_CUR);
    fscanf(plik,"%f", &RHO[i]);
    fseek(plik,4,SEEK_CUR);
    printf("%d\t%.5f\t\t%.5f\t\t%.5f\n", i, X[i],Y[i],RHO[i]);
    }
    return;
}

