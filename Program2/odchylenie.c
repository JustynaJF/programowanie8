#include <math.h>
#include "fun.h"

float odchylenie(float X[], float sr)
{
    float odchyl=0.0;
    float suma=0.0;

    for(int j=1;j<=50;j++)
    {
        suma+=pow(X[j]-sr,2);
    }
    suma=suma/50;
    odchyl=sqrt(suma);

    return odchyl;
}

    